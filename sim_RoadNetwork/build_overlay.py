from __future__ import division
import os, sys
from multiprocessing import Pool
import networkx as nx
import csv

sys.path.append(os.path.join(os.path.dirname('__file__'), '..'))
import build_network_fr as build_network
from tools import *
from settings import *

def traffic_assignment(G, paths, beta=1, gamma=1, utility="travelTime"):
    """ C-Logit traffic assignment
         - paths are the output of the top_shortest_paths function
         - utility is the utility unit (here, travel time on the traveled roads)
         - Returns the assignment on the paths/routes and the initial vehicle flow on the paths init_vehicle_flow
    """
    # Check whether the paths is empty
    if not paths:
        return [], 0

    init_vehicle_flow = 0.0
    assignment_sum = 0.0
    for path_id, path in paths.items():
        if path is None:
            continue
        utilityCost = sum([1/G[u][v][utility] for (u,v) in get_edges(path['path']) if G.has_edge(u,v) and G[u][v][utility] > 0.0])
        commonalityFactor = commonality_factor(G, path_id, paths, beta, gamma, utility)

        h_sum = 0.0
        for h_path_id, h_path in paths.items():
            h_utilityCost = sum([1/G[u][v][utility] for (u,v) in get_edges(h_path['path']) if G.has_edge(u,v) and G[u][v][utility] > 0.0])
            h_commonalityFactor = commonality_factor(G, h_path_id, paths, beta, gamma, utility)
            h_sum += math.exp(h_utilityCost - h_commonalityFactor)
        paths[path_id]['assignment'] = math.exp(utilityCost - commonalityFactor) / h_sum
        assignment_sum += paths[path_id]['assignment']

        # The initial vehicle flow is equal to the sum of the bottleneck of each path times the assignment on the path
        init_vehicle_flow += paths[path_id]['assignment'] * min([G[u][v]["aadt"] for (u,v) in get_edges(path['path']) if G[u][v]["aadt"] > 0.0])

        # print "\t", path_id, utilityCost, commonalityFactor, utilityCost - commonalityFactor, paths[path_id]['pathLength'], paths[path_id]['assignment']

    # print "Sum assignments: ", assignment_sum, init_vehicle_flow
    return paths, init_vehicle_flow


def execute_dijkstra((junction_i, junction_j)):
    global road_network
    try:
        (lOpt, Opt) = nx.single_source_dijkstra(road_network, junction_i, target=junction_j, weight='distance')
    except nx.NetworkXNoPath:
        return -1 # In case there is no path between junction_i and junction_j
    return lOpt[junction_j]

def create_trip_matrix_instance((junction_i, junction_j)):
    global road_network

    # Compute the alternative routes for the points i and j
    paths = top_shortest_paths(road_network, junction_i, junction_j, weight='distance', maxWeight=MAX_DIST)

    # Return the paths and the initial vehicle flow related to the traffic assignment from i to j
    # Recall that traffic_assignment: paths, init_vehicle_flow
    return traffic_assignment(road_network, paths)

def create_trip_matrix(G, points):
    """ Create a trip matrix from the graph "points" containing the
        origins and destinations of the trip matrix
    """

    matrixIndexes = []
    indexes = []

    for i in range(len(points.nodes())):
        junction_i = points.nodes()[i]
        for j in range(len(points.nodes())):
            junction_j = points.nodes()[j]

            # Compute the optimal path between junction_i and junction_j to discard the path if the distance is greater than maxDist
            indexes.append([junction_i, junction_j])

    print "run res_dijkstra with", len(indexes), "processes"
    pool = Pool(processes=NB_PROCESSES)
    res_dijkstra = pool.map(execute_dijkstra, indexes)
    for i in range(len(indexes)):
        junction_i, junction_j = indexes[i]
        if res_dijkstra[i] <= MAX_DIST and res_dijkstra[i] > 0:
            matrixIndexes.append([junction_i, junction_j])

    print "start multithreading with", len(matrixIndexes), "processes"
    trip_matrix = AutoVivification()
    for (point_i, point_j) in matrixIndexes:
        trip_matrix[point_i][point_j]["paths"], trip_matrix[point_i][point_j]["init_vehicle_flow"] = create_trip_matrix_instance((point_i, point_j))
        trip_matrix[point_i][point_j]["vehicle_flow_series"] = {}

    return trip_matrix


def create_trip_matrix_mutlithreaded(G, points):
    """ Create a trip matrix from the graph "points" containing the
        origins and destinations of the trip matrix, using the Multiprocessing library
    """

    matrixIndexes = []
    indexes = []

    trip_matrix = AutoVivification()
    for i in range(len(points.nodes())):
        junction_i = points.nodes()[i]
        for j in range(len(points.nodes())):
            junction_j = points.nodes()[j]

            # Compute the optimal path between junction_i and junction_j to discard the path if the distance is greater than maxDist
            indexes.append([junction_i, junction_j])

    print "run res_dijkstra with", len(indexes), "processes"
    pool = Pool(processes=NB_PROCESSES)
    res_dijkstra = pool.map(execute_dijkstra, indexes)
    for i in range(len(indexes)):
        junction_i, junction_j = indexes[i]
        if res_dijkstra[i] <= MAX_DIST and res_dijkstra[i] > 0:
            matrixIndexes.append([junction_i, junction_j])

    print "start multithreading with", len(matrixIndexes), "processes"
    # Get results from the multithreaded map
    res = pool.map(create_trip_matrix_instance, matrixIndexes)

    print "Unpack the results into the trip matrix"
    # Unpack the results and put them in the trip_matrix
    for i in range(len(res)):
        point_i, point_j = matrixIndexes[i]
        trip_matrix[point_i][point_j]["paths"], trip_matrix[point_i][point_j]["init_vehicle_flow"] = res[i]
        trip_matrix[point_i][point_j]["vehicle_flow_series"] = {}

    return trip_matrix

def OD_trip_matrix_init(G, trip_matrix):
    """ Derive the traffic assignment on each link for each edge of graph G
    """
    edgesToLookup = []
    edgesDict = AutoVivification()
    cpt_edges = 0
    edges_total = len(G.edges())
    for u,v, edge_data in G.edges(data=True):
        if edge_data['aadt'] <= 0.0:
            continue
        edge = tuple((u,v))
        sumTotalAssignment = 0.0
        if cpt_edges % 1000 == 0:
            print 100.0 * cpt_edges / edges_total
        for i, i_data in trip_matrix.items():
            for j, j_data in i_data.items():
                # for a given (i,j) cell of the trip matrix,
                # multiple paths may go through the edge
                pathList = []
                sumAssignment = 0.0

                for path_id, path_data in j_data['paths'].items():
                    if edge in get_edges(path_data['path']):
                        pathList.append(path_id)
                        sumAssignment += path_data['assignment']
                # if sumAssignment > 0.0:
                #     print G.node[i]['name'],G.node[j]['name'], G[edge[0]][edge[1]]['name'],sumAssignment,pathList
                sumTotalAssignment += sumAssignment
                edgesDict[edge][i][j]['assignment'] = sumAssignment
                edgesDict[edge][i][j]['paths'] = pathList

        # add the maximum aadt and the dual variable lambda
        edgesDict[edge]['aadt'] = edge_data['aadt']
        edgesDict[edge]['lambda'] = 0.0
        if sumTotalAssignment > 0.0:
            edgesToLookup.append(edge)
        cpt_edges += 1

    return edgesToLookup, edgesDict

def OD_trip_matrix(G, trip_matrix, error, edgesToLookup, edgesDict):
    """ Min entropy maximization from traffic counts
        Return the trip matrix based on the AADT of graph G.
         - trip_matrix consists of the paths that are computed by the alternative_routes
           function and asisgned by the traffic_assignment function.
           trip_matrix[gid_i][gid_j]["vehicle_flow"]
                                    ["paths"][path_id]["assignment"]
                                                      ["path"]
                                                      ["pathLength"]
                                                      ["functionValue"]
    """

    print "trip matrix iteration"
    cpt = 0
    cpt_false = 0
    flag = True
    maxFlow = 0.0
    maxFlow_cpt = 0
    maxFlow_cpt_false = 0
    while(cpt < 10000 and flag):
        print "Round", cpt, cpt_false, len(edgesToLookup)
        # Compute the estimated trip value T_ij
        sum_assigned_flows = 0.0
        for i, i_data in trip_matrix.items():
            for j, j_data in i_data.items():

                sum_flow = 0.0
                for edge in edgesToLookup:
                    sum_flow += edgesDict[edge]['lambda'] * edgesDict[edge][i][j]['assignment']
                trip_matrix[i][j]['vehicle_flow'] = math.fabs(sum_flow)
                trip_matrix[i][j]['vehicle_flow_series'][cpt] = math.fabs(sum_flow)
                sum_assigned_flows += trip_matrix[i][j]['vehicle_flow']

                print G.node[i]['name'], G.node[j]['name'], trip_matrix[i][j]['vehicle_flow'], trip_matrix[i][j]['vehicle_flow_series'][cpt]


        cpt_false = 0
        for edge in edgesToLookup:
            sum_flow_path = 1.0
            for i, i_data in trip_matrix.items():
                for j, j_data in i_data.items():
                    sum_flow_path += trip_matrix[i][j]['vehicle_flow'] * edgesDict[edge][i][j]['assignment']
            # print "\t", sum_flow_path, edgesDict[edge]['aadt']
            if edgesDict[edge]['aadt'] * (1.0-error) <= sum_flow_path <= edgesDict[edge]['aadt'] * (1.0+error):
                cpt_false += 1
                # print sum_flow_path, edgesDict[edge]['aadt']
            else:
                # print "\t", sum_flow_path, edgesDict[edge]['lambda'], mpmath.log(edgesDict[edge]['aadt']), mpmath.log(sum_flow_path)
                edgesDict[edge]['lambda'] = edgesDict[edge]['lambda'] + math.log(edgesDict[edge]['aadt']) - math.log(math.fabs(sum_flow_path))
                # print edgesDict[edge]['lambda'], math.log(sum_flow_path), math.log(edgesDict[edge]['aadt'])

        if cpt_false >= 0.15 * len(edgesToLookup):
            flag = False
        if cpt_false >= maxFlow_cpt_false:
            if sum_assigned_flows > maxFlow:
                maxFlow_cpt_false = cpt_false
                maxFlow_cpt = cpt
                maxFlow = sum_assigned_flows
        cpt += 1

    print "Final value is", maxFlow, maxFlow_cpt, maxFlow_cpt_false

    # Choose the best flow for all the (origine, destination) pairs
    for i, i_data in trip_matrix.items():
        for j, j_data in i_data.items():
            trip_matrix[i][j]['vehicle_flow'] = trip_matrix[i][j]['vehicle_flow_series'][maxFlow_cpt]

    return trip_matrix

def offloading_overlay_characterization(road_network, trip_matrix, leakage):
    """ Returns the offloading overlay digraph from the trip matrix calaculated
        trip_matrix[gid_i][gid_j]["vehicle_flow"]
                                 ["weightedDist"]
                                 ["weightedTravelTime"]
                                 ["paths"][path_id]["assignment"]
                                                   ["path"]
                                                   ["pathLength"]
                                                   ["pathTime]
                                                   ["functionValue"]
    """
    G = nx.DiGraph()
    edge_id = 1
    for i, i_data in trip_matrix.items():
        for j, j_data in i_data.items():

            # Check the paths are in the trip matrix (they may be inexistent, due to the fact the trip matrix data has been loaded from a file)
            if j_data['paths']:

                # Discard the link if the shortest path distance is greater than maxDist
                if j_data['paths'][0]['pathLength'] > MAX_DIST or j_data['paths'][0]['pathLength'] <= 0:
                    continue

                # Compute the weighted travel time and distance of the flows on the considered paths between i and j
                weightedDist = 0.0
                weightedTravelTime = 0.0
                for path_id, path_data in j_data['paths'].items():
                    weightedDist += path_data['assignment'] * path_data['pathLength']
                    weightedTravelTime += path_data['assignment'] * path_data['pathTime']

                trip_matrix[i][j]['weightedDist'] = weightedDist
                trip_matrix[i][j]['weightedTravelTime'] = weightedTravelTime

            # Compute the resulting throughput on the overlay link (i,j)
            throughput = (STORAGE_SIZE * j_data['vehicle_flow'] * PENETRATION_RATIO) / (3600 * 24)
            # print weightedDist, weightedTravelTime, throughput / 1e9

            node_i = road_network.node[i]['name']
            node_j = road_network.node[j]['name']
            edge_name = 'll-%d' % (edge_id)

            # Add the edge (i,j) in the overlay digraph
            G.add_edge(node_i,node_j,
                        distance     = j_data['weightedDist'],
                        travelTime   = j_data['weightedTravelTime'],
                        throughput   = math.fabs(throughput),
                        flows        = [],
                        leakage      = leakage,
                        rem_capacity = math.fabs(throughput),
                        traffic      = math.fabs(j_data['vehicle_flow']*PENETRATION_RATIO),
                        name         = edge_name+'-a')
            G.add_edge(node_j,node_i,
                        distance     = j_data['weightedDist'],
                        travelTime   = j_data['weightedTravelTime'],
                        throughput   = math.fabs(throughput),
                        flows        = [],
                        leakage      = leakage,
                        rem_capacity = math.fabs(throughput),
                        traffic      = math.fabs(j_data['vehicle_flow']*PENETRATION_RATIO),
                        name         = edge_name+'-b')
            edge_id += 1

    return G

def get_trip_matrix_from_file(filename, trip_matrix=None):
    """ Read the trip matrix from filename """
    if not trip_matrix:
        trip_matrix = AutoVivification() # Instanciate a new trip matrix
    with open(filename, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            gid_i = (float(row[1]), float(row[2]))
            gid_j = (float(row[3]), float(row[4]))
            trip_matrix[gid_i][gid_j]["vehicle_flow"] = float(row[8])
            trip_matrix[gid_i][gid_j]["weightedDist"] = float(row[5])
            trip_matrix[gid_i][gid_j]["weightedTravelTime"] = float(row[6])

    return trip_matrix

def generate_routefile(G, trip_matrix, out_filename="sim.rou.xml"):
    """ This function generates the route file from the attributes given in the command line """

    with open(out_filename, "w") as routes:
        print >> routes, """<routes>
    <!-- Types of vehicles -->
    <vTypeDistribution id="typedist1">"""
        print >> routes, '        <vType id="type1" vClass="passenger" probability="%.2f" color="0,0,1" />' % (PENETRATION_RATIO)
        print >> routes, '        <vType id="type2" vClass="passenger" probability="%.2f" color="0,1,1" />' % (1.0-PENETRATION_RATIO)
        print >> routes, "    </vTypeDistribution>"

        print >> routes, '    <!-- Flows -->'
        flow_sum = 0
        for i, i_data in trip_matrix.items():
            for j, j_data in i_data.items():

                i_name, j_name = G.node[i]['name'], G.node[j]['name']

                # Define a route distribution
                route_dist_name = 'routedist-%s-%s' % (i_name, j_name)
                print >> routes, '    <routeDistribution id="%s">' % (route_dist_name)
                for path_id, path_data in j_data['paths'].items():
                    route = " ".join([G[u][v]['name'] for (u,v) in get_edges(path_data['path'])])
                    route_name = 'route-%s-%s-%d' % (i_name, j_name, path_id)
                    route_proba = path_data['assignment']
                    print >> routes, '        <route id="%s" edges="%s" probability="%g"/>' % (route_name, route, route_proba)
                print >> routes, "    </routeDistribution>"

                # Define a flow with the previous route distribution
                flow_name = 'flow-%s-%s' % (i_name, j_name)
                traffic = math.fabs(j_data['vehicle_flow']*PENETRATION_RATIO)
                if traffic > 0:
                    print >> routes, '    <flow id="%s" departPos="0" departLane="random" departSpeed="max" type="typedist1" route="%s" begin="0.00" vehsPerHour="%g" />' % (flow_name, route_dist_name, traffic/24)
        print >> routes, '</routes>'

if __name__ == "__main__":
    print "Generate the France and overlay graphs"
    road_network = build_network.generate_graph("data/FR/traffic-fr-2011.xls")
    points = build_network.analyze_chosen_stations(road_network, 'data/FR/stations-fr-150km.csv')

    print "#### Compute trip matrix with", NB_PROCESSES, "processes"
    trip_matrix = create_trip_matrix_mutlithreaded(road_network, points)

    print "### Get trip matrix from file"
    trip_matrix = get_trip_matrix_from_file('data/FR/fr-overlay.txt', trip_matrix)

    print "### Dump SUMO route"
    generate_routefile(road_network, trip_matrix)

    # print "#### Derive each link assignment"
    # edgesToLookup, edgesDict = OD_trip_matrix_init(road_network, trip_matrix)

    # print "#### Compute flow for each (origin, destination) pair"
    # trip_matrix = OD_trip_matrix(road_network, trip_matrix, 0.10, edgesToLookup, edgesDict)

    print "#### Overlay link characterization"
    Overlay = offloading_overlay_characterization(road_network, trip_matrix, LEAKAGE)
