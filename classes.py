import random
import bisect

class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

class Data(object):
    """ This class represents a chunk of data that a vehicle can carry """
    def __init__(self, demand, seq=-1, startTime=0):
        self.demand = demand # demand of the data carried by the vehicle
        self.seq = seq
        self.startTime = startTime
        self._loadTime = startTime
        self.stops = AutoVivification() # Format: {node: {'dropoff': start_stop}, {'pickup': end_stop}, ...}
        self.endTime = -1
        self.isRetransmitted = False

    def getDemand(self):
        return self.demand

    @property
    def loadTime(self):
        return self._loadTime
    @loadTime.setter

    def loadTime(self, loadTime):
        if not self.stops[self.demand.getOrigin()]['pickup']:
            self.stops[self.demand.getOrigin()]['pickup'] = []
        self.stops[self.demand.getOrigin()]['pickup'].append(loadTime)
        self._loadTime = loadTime

    def startStop(self, node, step):
        if not self.stops[node]['dropoff']:
            self.stops[node]['dropoff'] = []
        self.stops[node]['dropoff'].append(step)

    def endStop(self, node, step):
        if not self.stops[node]['pickup']:
            self.stops[node]['pickup'] = []
        self.stops[node]['pickup'].append(step)

    def getStop(self, node, stop_type):
        """ Get the latest stop """
        if stop_type not in ['pickup', 'dropoff']: return None
        if not self.stops[node][stop_type]: return None
        return self.stops[node][stop_type][-1]

    def __eq__(self, other): 
        return (self.demand == other.demand) and (self.seq == other.seq)

    def __str__(self):
        return "# Data " + str(self.demand) + " " + str(self.seq) + "  #"

class Vehicle(object):
    """ This class represents a vehicle that is equipped with data storage device (of type1) """
    def __init__(self, vehID, vehOrigin, vehTarget, vehLogicalLink):
        self.vehID = vehID
        self.data = None
        self.sharesGPSInfo = True
        self.vehOrigin = vehOrigin
        self.vehTarget = vehTarget
        self.vehLogicalLink = vehLogicalLink

    def hasData(self):
        return (self.data != None)

    def loadData(self, data):
        self.data = data

    def unloadData(self):
        dat = self.data
        self.data = None
        return dat

    def getAllStops(self):
        return traci.vehicle.getStops(self.vehID)

    def getData(self):
        return self.data

    def getLogicalLink(self):
        return self.vehLogicalLink

    def getTarget(self):
        return self.vehTarget

    def getOrigin(self):
        return self.vehOrigin

    def isSharingGPSInfo(self):
        return self.sharesGPSInfo

    def __str__(self):
        return "# Vehicle " + str(self.vehID) + " with data " + self.data + " #"

class Demand(object):
    """ This class describes a demand to offload data onto the road network """
    def __init__(self, name, origin, target, rate):
        super(Demand, self).__init__()
        self.name = name
        self.origin = origin
        self.target = target
        self.rate = rate
        self.paths = AutoVivification()
        self.counter = 0 # Count the data generated

    def getOrigin(self):
        return self.origin

    def getTarget(self):
        return self.target

    def getPaths(self):
        return self.paths

    def getPath(self, path_name):
        return self.paths[path_name]

    def getRate(self):
        return self.rate

    def incCounter(self):
        self.counter += 1
        return self.counter

    def addPath(self, path_name, path):
        self.paths[path_name]['path'] = path

    def setPathRet(self, path_name, retransmissions):
        self.paths[path_name]['retransmissions'] = retransmissions

    def getPathRet(self, path_name):
        return self.paths[path_name]['retransmissions']

    def setPathFlow(self, path_name, flow):
        self.paths[path_name]['flow'] = flow

    def getPathFlow(self, path_name):
        return self.path[path_name]['flow']

    def __str__(self):
        return "# Demand " + str(self.name) + "("+str(self.origin)+", "+str(self.target)+") #"
