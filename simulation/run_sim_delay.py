#!/usr/bin/env python
from __future__ import division
import os, sys
import subprocess
import shlex
import csv
from math import floor
import numpy as np

class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

schedulers = ['RR', 'BUF', 'TRF', 'TRF1']
# paramters are (b,a) <=> (split ratio, gain)
parameters = [(0.25,1), (0.1,1), (0.5, 1), (0.5,0.5), (0.25,0.5), (0.1,0.5),(0.5,2), (0.25,2), (0.1,2)]
beta = [0.1, 0.25, 0.5]

Rscript = "/usr/bin/Rscript"

# need to check whether rate1 and rate2 are smaller than the previous 

for sch in schedulers:
	in_filename  = "results2/throughput_inf_"+sch+".txt"

	res = AutoVivification()
	with open(in_filename, 'r') as f:
		reader = csv.reader(f, delimiter=';')
		for row in reader:
			target = row[0]
			gamma_S, gamma_I = float(row[1]), float(row[2])
			split_ratio, alpha = float(row[3]), float(row[4])
			max_throughput = float(row[6])
			res[split_ratio][alpha][target] = floor(max_throughput * 100) / 100.0

	for (split_ratio,alpha) in parameters:
		out_filename = "results2/delay_"+sch+"_"+str(split_ratio)+"_"+str(alpha)+".txt"
		max_throughput_1 = res[split_ratio][alpha]['T1']
		max_throughput_2 = res[split_ratio][alpha]['T2']

		for b in beta:
			for rate_1 in np.linspace(0.01, max_throughput_1, num=10):
				rate_2 = b * rate_1 # rate2 = beta * rate1
				print "We have:", max_throughput_1, max_throughput_2, rate_1, rate_2, b


				if (rate_1 > max_throughput_1) or (rate_2 > max_throughput_2):
					continue 

				cmd = sys.executable + " runner2.py "+out_filename+" "+str(alpha)+" "+str(split_ratio)+" "+sch+" Poisson "+str(rate_1)+" "+str(b)
				print cmd
				subprocess.call(shlex.split(cmd))
		cmd = Rscript + " results/make_plot_delay.r " + out_filename
		# print cmd
		# subprocess.call(shlex.split(cmd))
